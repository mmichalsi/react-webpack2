import Core from "./components/Core";

function errorLoading(err) {
    throw new Error(`Dynamic page loading failed ${err}`);
}

function loadRoute(routeCallback) {
    return module => routeCallback(null, module.default)
}

export default {
    path: "/",
    component: Core,
    indexRoute: {
        getComponent(location, callback) {
            System.import("./components/Home")
                .then(loadRoute(callback))
                .catch(errorLoading);
        }
    },
    childRoutes: [
        {
            path: "about",
            getComponent(location, callback) {
                System.import("./components/About")
                    .then(loadRoute(callback))
                    .catch(errorLoading);
            }
        },
        {
            path: "users",
            getComponent(location, callback) {
                System.import("./components/Users")
                    .then(loadRoute(callback))
                    .catch(errorLoading);
            }
        },
        {
            path: "home",
            getComponent(location, callback) {
                System.import("./components/Home")
                    .then(loadRoute(callback))
                    .catch(errorLoading);
            }
        }
    ]
}